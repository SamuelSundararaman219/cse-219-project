package oh.clipboard;

import static djf.AppPropertyType.COPY_BUTTON;
import static djf.AppPropertyType.CUT_BUTTON;
import static djf.AppPropertyType.PASTE_BUTTON;
import djf.components.AppClipboardComponent;
import djf.modules.AppFoolproofModule;
import djf.modules.AppGUIModule;
import java.util.ArrayList;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import oh.OfficeHoursApp;
import static oh.OfficeHoursPropertyType.OH_TAS_TABLE_VIEW;
import oh.data.OfficeHoursData;
import oh.data.TeachingAssistantPrototype;
import oh.workspace.foolproof.OfficeHoursFoolproofDesign;

/**
 *
 * @author McKillaGorilla
 */
public class OfficeHoursClipboard implements AppClipboardComponent {
    OfficeHoursApp app;
    ArrayList<TeachingAssistantPrototype> clipboardCutItems;
    ArrayList<TeachingAssistantPrototype> clipboardCopiedItems;
    
    public OfficeHoursClipboard(OfficeHoursApp initApp) {
        app = initApp;
        clipboardCutItems = null;
        clipboardCopiedItems = null;
        
    }
    
    @Override
    public void cut() {
        
        AppGUIModule gui = app.getGUIModule();
        TableView taTableView = (TableView) gui.getGUINode(OH_TAS_TABLE_VIEW);
        OfficeHoursData data = (OfficeHoursData) app.getDataComponent();
            TeachingAssistantPrototype ta = (TeachingAssistantPrototype) taTableView.getSelectionModel().getSelectedItem();
            this.clipboardCutItems.add(ta);
            data.removeTA(ta);
            
        
    }

    @Override
    public void copy() {
        AppGUIModule gui = app.getGUIModule();
        TableView taTableView = (TableView) gui.getGUINode(OH_TAS_TABLE_VIEW);
        OfficeHoursData data = (OfficeHoursData) app.getDataComponent();
            TeachingAssistantPrototype ta = (TeachingAssistantPrototype) taTableView.getSelectionModel().getSelectedItem();
            this.clipboardCopiedItems.add(ta);
            
        
    }
    
    @Override
    public void paste() {
        AppGUIModule gui = app.getGUIModule();
        TableView taTableView = (TableView) gui.getGUINode(OH_TAS_TABLE_VIEW);
        TeachingAssistantPrototype ta = (TeachingAssistantPrototype) this.clipboardCopiedItems.get(this.clipboardCopiedItems.size()-1);
        OfficeHoursData data = (OfficeHoursData) app.getDataComponent();
        data.addTA(ta);
        
        
            
            
        
    }    


    @Override
    public boolean hasSomethingToCut() {
        return ((OfficeHoursData)app.getDataComponent()).isTASelected();
    }

    @Override
    public boolean hasSomethingToCopy() {
        return ((OfficeHoursData)app.getDataComponent()).isTASelected();
    }

    @Override
    public boolean hasSomethingToPaste() {
        if ((clipboardCutItems != null) && (!clipboardCutItems.isEmpty()))
            return true;
        else if ((clipboardCopiedItems != null) && (!clipboardCopiedItems.isEmpty()))
            return true;
        else
            return false;
    }
}