package oh.transactions;

import jtps.jTPS_Transaction;
import oh.clipboard.OfficeHoursClipboard;
import oh.data.OfficeHoursData;
import oh.data.TeachingAssistantPrototype;

public class CutTA_Transaction implements jTPS_Transaction {
    OfficeHoursData data;
    OfficeHoursClipboard clipboard;
    TeachingAssistantPrototype ta;
    
    public CutTA_Transaction(OfficeHoursData initData, TeachingAssistantPrototype initTA, OfficeHoursClipboard initBoard) {
        data = initData;
        ta = initTA;
        clipboard = initBoard;
    }

    @Override
    public void doTransaction() {
        clipboard.cut();        
    }

    @Override
    public void undoTransaction() {
        clipboard.paste();
    }
}