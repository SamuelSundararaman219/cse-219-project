package oh.transactions;

import jtps.jTPS_Transaction;
import oh.data.OfficeHoursData;
import oh.data.TeachingAssistantPrototype;


public class EditTA_Transaction implements jTPS_Transaction {
    OfficeHoursData data;
    TeachingAssistantPrototype ta;
    String name;
    String email;
    String type;
    String orgName;
    String orgEmail;
    String orgType;
    
    public EditTA_Transaction(OfficeHoursData initData, TeachingAssistantPrototype initTA, String initName, String initEmail, String initType) {
        data = initData;
        ta = initTA;
        name = initName;
        email = initEmail;
        type = initType;
        orgName = ta.getName();
        orgEmail = ta.getEmail();
        orgType = ta.getType();
    }

    @Override
    public void doTransaction() {
        
        data.editTA(ta, name, email, type);        
    }

    @Override
    public void undoTransaction() {
        
        data.editTA(data.getTAWithName(name), orgName, orgEmail, orgType);
    }
}
