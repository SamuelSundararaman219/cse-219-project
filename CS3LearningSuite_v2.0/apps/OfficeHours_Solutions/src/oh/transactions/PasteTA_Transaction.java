package oh.transactions;

import jtps.jTPS_Transaction;
import oh.clipboard.OfficeHoursClipboard;
import oh.data.OfficeHoursData;
import oh.data.TeachingAssistantPrototype;


public class PasteTA_Transaction implements jTPS_Transaction {
    OfficeHoursData data;
    TeachingAssistantPrototype ta;
    OfficeHoursClipboard clipboard;
    
    public PasteTA_Transaction(OfficeHoursData initData, TeachingAssistantPrototype initTA, OfficeHoursClipboard initBoard) {
        data = initData;
        ta = initTA;
        clipboard = initBoard;
    }

    @Override
    public void doTransaction() {
        clipboard.paste();        
    }

    @Override
    public void undoTransaction() {
        clipboard.cut();
    }
}
