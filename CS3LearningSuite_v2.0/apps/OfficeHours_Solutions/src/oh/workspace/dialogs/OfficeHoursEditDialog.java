package oh.workspace.dialogs;

import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import oh.OfficeHoursApp;
import static oh.OfficeHoursPropertyType.OH_DIALOG_EMAIL_BOX;
import static oh.OfficeHoursPropertyType.OH_DIALOG_NAME_BOX;
import oh.data.OfficeHoursData;
import oh.data.TeachingAssistantPrototype;
import oh.transactions.EditTA_Transaction;
import properties_manager.PropertiesManager;

/**
 * @author McKillaGorilla
 */
public class OfficeHoursEditDialog {

    public static void showEditDialog(OfficeHoursApp initApp, Stage parent, Object titleProperty, TeachingAssistantPrototype ta, OfficeHoursData initData) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        OfficeHoursApp app = initApp;
        String title = props.getProperty(titleProperty);
        Dialog<TeachingAssistantPrototype> dialog = new Dialog<>();
        DialogPane dialogPane = dialog.getDialogPane();
        dialog.initOwner(parent);
        dialog.setTitle("");
        dialog.setHeaderText(title);
        dialogPane.getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
        HBox nameBox = new HBox();
        HBox emailBox = new HBox();
        Label name = new Label("Name: ");
        Label email = new Label("Email: ");
        TextField nameField = new TextField(ta.getName()); 
        TextField emailField = new TextField(ta.getEmail());
        final ToggleGroup group = new ToggleGroup();
        HBox radioBox = new HBox();
        RadioButton undergrad = new RadioButton();
        undergrad.setToggleGroup(group);
        undergrad.setText("Undergraduate");
        RadioButton grad = new RadioButton();
        grad.setToggleGroup(group);
        grad.setText("Graduate");
        radioBox.getChildren().add(undergrad);
        radioBox.getChildren().add(grad);
        nameBox.getChildren().add(name);
        nameBox.getChildren().add(nameField);
        emailBox.getChildren().add(email);
        emailBox.getChildren().add(emailField);
        nameBox.setSpacing(5);
        emailBox.setSpacing(5);
        if(ta.getType().equals("Undergraduate")){
            undergrad.setSelected(true);
        }
        else{
            grad.setSelected(true);
        }
        dialogPane.setContent(new VBox(8,nameBox,emailBox,radioBox));
        dialog.setResultConverter((ButtonType button) -> {
            if(button ==ButtonType.OK){
                String initName = nameField.getText();
                String initEmail = emailField.getText();
                String initType ="";
                if(group.getSelectedToggle().equals(undergrad)){
                    initType = "Undergraduate";
                }
                else{
                    initType = "Graduate";
                }
                EditTA_Transaction transaction = new EditTA_Transaction(initData,ta, initName, initEmail, initType);
                app.processTransaction(transaction);
                
               
            }
            return null;
        });
        
        dialog.showAndWait();
        

       
    }
}