package oh.workspace.foolproof;

import static djf.AppPropertyType.COPY_BUTTON;
import static djf.AppPropertyType.CUT_BUTTON;
import static djf.AppPropertyType.PASTE_BUTTON;
import djf.components.AppClipboardComponent;
import djf.modules.AppGUIModule;
import djf.ui.foolproof.FoolproofDesign;
import java.util.Collections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import oh.OfficeHoursApp;
import static oh.OfficeHoursPropertyType.OH_ADD_TA_BUTTON;
import static oh.OfficeHoursPropertyType.OH_DIALOG_EMAIL_BOX;
import static oh.OfficeHoursPropertyType.OH_DIALOG_NAME_BOX;
import static oh.OfficeHoursPropertyType.OH_EMAIL_TEXT_FIELD;
import static oh.OfficeHoursPropertyType.OH_NAME_TEXT_FIELD;
import static oh.OfficeHoursPropertyType.OH_RADIO_PANE;
import oh.clipboard.OfficeHoursClipboard;
import oh.data.OfficeHoursData;

/**
 *
 * @author McKillaGorilla
 */
public class OfficeHoursFoolproofDesign implements FoolproofDesign {
    OfficeHoursApp app;
    
    public OfficeHoursFoolproofDesign(OfficeHoursApp initApp) {
        app = initApp;
    }

    @Override
    public void updateControls() {
        updateAddTAFoolproofDesign();
        //updateEditTAFoolproofDesign();
        updateRadioFoolproofDesign();
        //updateClipboardFoolproofDesign();
    }                              
    
    
    
    public void updateRadioFoolproofDesign(){
        AppGUIModule gui = app.getGUIModule();
        TextField nameTextField = ((TextField) gui.getGUINode(OH_NAME_TEXT_FIELD));
        TextField emailTextField = ((TextField) gui.getGUINode(OH_EMAIL_TEXT_FIELD));
        Button addTAButton = (Button)gui.getGUINode(OH_ADD_TA_BUTTON);
        HBox radioBox = (HBox) gui.getGUINode(OH_RADIO_PANE);
        RadioButton all = (RadioButton) radioBox.getChildren().get(0);
        RadioButton undergrad = (RadioButton) radioBox.getChildren().get(1);
        RadioButton grad = (RadioButton) radioBox.getChildren().get(2);
        ToggleGroup group = all.getToggleGroup();
        if(group.getSelectedToggle().equals(all)){
            nameTextField.setOnAction(null);
            emailTextField.setOnAction(null);
            addTAButton.setDisable(true);
        }
        
    }
    
    public void updateEditTAFoolproofDesign(){
        AppGUIModule gui = app.getGUIModule();
        TextField nameTextField = ((TextField) gui.getGUINode(OH_DIALOG_NAME_BOX));
        TextField emailTextField = ((TextField) gui.getGUINode(OH_DIALOG_EMAIL_BOX));
        String name = nameTextField.getText();
        String email = emailTextField.getText();
        OfficeHoursData data = (OfficeHoursData) app.getDataComponent();
        Button addTAButton = (Button)gui.getGUINode(OH_ADD_TA_BUTTON);
        boolean isLegal = data.isLegalNewTA(name, email);
        ObservableList<String> nameStyle = nameTextField.getStyleClass();
        ObservableList<String> emailStyle = emailTextField.getStyleClass();
        if (isLegal) {
            nameTextField.setOnAction(addTAButton.getOnAction());
            emailTextField.setOnAction(addTAButton.getOnAction());
            if(nameStyle.contains("error")){
                nameStyle.removeAll(Collections.singleton("error"));
            }
            if(emailStyle.contains("error")){
                emailStyle.removeAll(Collections.singleton("error"));
            }
        }
        else {
            nameTextField.setOnAction(null);
            emailTextField.setOnAction(null);
            if(data.isLegalName(name)&&!data.isLegalNewEmail(email)){
                emailStyle.add("error");
                if(nameStyle.contains("error")){
                    nameStyle.removeAll(Collections.singleton("error"));
                }
            }
            if(!data.isLegalName(name)&&data.isLegalNewEmail(email)){
                nameStyle.add("error");
                if(emailStyle.contains("error")){
                emailStyle.removeAll(Collections.singleton("error"));
                }
            }
            if(!data.isLegalName(name)&&!data.isLegalNewEmail(email)){
                nameStyle.add("error");
                emailStyle.add("error");
            }
        }
        addTAButton.setDisable(!isLegal);
    }

    public void updateAddTAFoolproofDesign() {
        AppGUIModule gui = app.getGUIModule();
        // FOOLPROOF DESIGN STUFF FOR ADD TA BUTTON
        
        
        TextField nameTextField = ((TextField) gui.getGUINode(OH_NAME_TEXT_FIELD));
        TextField emailTextField = ((TextField) gui.getGUINode(OH_EMAIL_TEXT_FIELD));
        String name = nameTextField.getText();
        String email = emailTextField.getText();
        OfficeHoursData data = (OfficeHoursData) app.getDataComponent();
        Button addTAButton = (Button)gui.getGUINode(OH_ADD_TA_BUTTON);
        boolean isLegal = data.isLegalNewTA(name, email);
        ObservableList<String> nameStyle = nameTextField.getStyleClass();
        ObservableList<String> emailStyle = emailTextField.getStyleClass();
        if (isLegal) {
            nameTextField.setOnAction(addTAButton.getOnAction());
            emailTextField.setOnAction(addTAButton.getOnAction());
            if(nameStyle.contains("error")){
                nameStyle.removeAll(Collections.singleton("error"));
            }
            if(emailStyle.contains("error")){
                emailStyle.removeAll(Collections.singleton("error"));
            }
        }
        else {
            nameTextField.setOnAction(null);
            emailTextField.setOnAction(null);
            if(data.isLegalName(name)&&!data.isLegalNewEmail(email)){
                emailStyle.add("error");
                if(nameStyle.contains("error")){
                    nameStyle.removeAll(Collections.singleton("error"));
                }
            }
            if(!data.isLegalName(name)&&data.isLegalNewEmail(email)){
                nameStyle.add("error");
                if(emailStyle.contains("error")){
                emailStyle.removeAll(Collections.singleton("error"));
                }
            }
            if(!data.isLegalName(name)&&!data.isLegalNewEmail(email)){
                nameStyle.add("error");
                emailStyle.add("error");
            }
        }
        addTAButton.setDisable(!isLegal);
    }

    public void updateClipboardFoolproofDesign() {
        OfficeHoursClipboard clipboard =  (OfficeHoursClipboard) app.getClipboardComponent();
        AppGUIModule gui = app.getGUIModule();
        Button cutButton = (Button)gui.getGUINode(CUT_BUTTON);
        cutButton.setDisable(!clipboard.hasSomethingToCut());
        Button copyButton = (Button)gui.getGUINode(COPY_BUTTON);
        copyButton.setDisable(!clipboard.hasSomethingToCopy());
        Button pasteButton = (Button)gui.getGUINode(PASTE_BUTTON);
        pasteButton.setDisable(!clipboard.hasSomethingToPaste());
        
    }
    
}